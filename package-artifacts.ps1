$modVersion = (Get-Content ".\src\mod_info.json" | ConvertFrom-Json).version
$releasePath = ".\out\release\SpeedUp_$($modVersion).7z"

Remove-Item $releasePath | Out-Null
7z a $releasePath ".\out\artifacts\SpeedUp"